import puppeteer from 'puppeteer';

export default async function getItemData(url) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.setViewport({ width: 1920, height: 1080 });
  await page.goto(url);

  const scrapeItemData = await page.evaluate(() => {
    let data = {};

    data.name = document.querySelector(
      'h1[data-testid="product-name"]'
    ).innerText;

    data.price = document.querySelector(
      'div[data-selen="product-price"]'
    ).innerText;

    data.description = document.querySelector(
      'div[class*="content-descriptionstyled"] > div:not([class])'
    ).innerText;

    data.sizes = Array.from(
      document.querySelectorAll('span[data-testid="size-name"]')
    ).map((size) => size.innerHTML);

    data.colors = Array.from(
      document.querySelectorAll(
        'img[data-testid="color-picker-list-item-color"]'
      )
    ).map((colorImg) => colorImg.title);

    data.images = Array.from(
      document.querySelectorAll('li[class*="thumbnailstyled"] > img')
    ).map((imageTag) => {
      const imageUrl = imageTag.src;
      return {
        small: imageUrl,
        large: imageUrl.replace('/160/', '/1200/'),
      };
    });

    return data;
  });

  const result = scrapeItemData;

  await browser.close();

  return result;
}
