import puppeteer from "puppeteer";

const url = 'https://www.mohito.com/pl/pl/';

export default async function getCategoryLinks() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);
  
    const scrapeCategoryLinks = await page.evaluate(() => {
      let links = {};
      const categories = ['Ubrania', 'Akcesoria'];
      for (let category of categories) {
        links[category] = {};
        const categoryItems = document.querySelectorAll(
          'li[data-testid="category-link"]'
        );
        categoryItems.forEach((item) => {
          if (item.querySelector('a').innerText === category) {
            const subcategoryItems = item.querySelectorAll(
              `li[data-testid="subcategory-link"] > a`
            );
            subcategoryItems.forEach((subcategoryItem) => {
              if (
                subcategoryItem.innerText !== 'Bestsellery' &&
                subcategoryItem.innerText !== 'Ponownie dostępne' &&
                subcategoryItem.innerText !== 'Wkrótce w sprzedaży!' &&
                subcategoryItem.innerText !== 'Zobacz wszystko' &&
                subcategoryItem.innerText !== 'Wyjątkowe prezenty'
              )
                links[category][subcategoryItem.innerText] = subcategoryItem.href;
            });
          }
        });
      }
  
      return links;
    });
  
    const result = scrapeCategoryLinks;
    
    await browser.close();

    return result;
}