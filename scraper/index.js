import getCategoryLinks from './getCategoryLinks.js';
import getItemsLinks from './getItemsLinks.js';
import getItemData from './getItemData.js';
import fs from 'fs';

const main = async () => {
  const categoryLinks = await getCategoryLinks();
  console.log('topCategory links', categoryLinks);

  const productLinks = {};

  for (const topCategory of Object.keys(categoryLinks)) {
    productLinks[topCategory] = {};

    for (const subCategory of Object.keys(categoryLinks[topCategory])) {
      productLinks[topCategory][subCategory] = [];
      const link = categoryLinks[topCategory][subCategory];
      const responseLink = await getItemsLinks(link);
      productLinks[topCategory][subCategory] = [
        ...productLinks[topCategory][subCategory],
        ...responseLink,
      ];
      console.log('product links', productLinks);
      fs.writeFileSync('product_links.json', JSON.stringify(productLinks));
    }
  }

  const productsData = [];
  for (const topCategory of Object.keys(productLinks)) {
    for (const subCategory of Object.keys(productLinks[topCategory])) {
      const linksArray = productLinks[topCategory][subCategory];
      for (const link of linksArray) {
        const product = await getItemData(link);
        const newProduct = {
          name: product.name,
          price: product.price,
          priceNetto: (
            (
              parseFloat(
                product.price.replace(/[^\d.,]/g, '').replace(',', '.')
              ) / 1.23
            ).toFixed(2) + ' PLN'
          ).replace('.', ','),
          category: `${topCategory}/${subCategory}`,
          description: product.description,
          images: product.images,
        };
        productsData.push(newProduct);
        console.log('products data', productsData);
        fs.writeFileSync('scraper_data.json', JSON.stringify(productsData));
      }
    }
  }
};

main();
