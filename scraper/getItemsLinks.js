import puppeteer from 'puppeteer';

export default async function getItemsLinks(url) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);

  const scrapeItemsLinks = await page.evaluate(() => {
    let links = [];
    const items = document.querySelectorAll('figure > a');
    
    for (let item of items) {
      if (links.length > 30) {
        break;
      }
      links.push(item.href);
    }
    
    return links;
  });

  const result = scrapeItemsLinks;

  await browser.close();

  return result;
}
