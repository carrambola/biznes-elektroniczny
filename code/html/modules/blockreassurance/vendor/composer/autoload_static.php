<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite4915e543bcbc74a6df91a0c70ed146e
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PrestaShop\\Module\\BlockReassurance\\' => 35,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PrestaShop\\Module\\BlockReassurance\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'ReassuranceActivity' => __DIR__ . '/../..' . '/classes/ReassuranceActivity.php',
        'blockreassurance' => __DIR__ . '/../..' . '/blockreassurance.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite4915e543bcbc74a6df91a0c70ed146e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite4915e543bcbc74a6df91a0c70ed146e::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite4915e543bcbc74a6df91a0c70ed146e::$classMap;

        }, null, ClassLoader::class);
    }
}
