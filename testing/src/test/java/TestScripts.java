import org.example.TestUtils.AddMultipleToCartUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestScripts {
    private static WebDriver driver;
    private static FluentWait<WebDriver> fluentWait;


    @BeforeAll
    public static void initDriver() {


        System.setProperty("webdriver.chrome.driver", "C:\\Users\\grzes\\projects\\biznes-elektroniczny\\drivers\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--ignore-ssl-errors=yes");
        options.addArguments("--ignore-certificate-errors");
        driver = new ChromeDriver(options);
        fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class);
        driver.manage().window().maximize();
        driver.get("https://localhost/index.php");
    }

    @Test
    @Order(1)
    public void addProductFromDifferentCategoriesListsTest() {
        AddMultipleToCartUtils utils = new AddMultipleToCartUtils(driver);
        utils.navigateToDresses(driver);
        utils.addToCartFromTheListWithoutIncrease(driver,1, fluentWait);
        utils.addToCartFromTheListWithoutIncrease(driver, 2, fluentWait);
        utils.addToCartFromTheListWithoutIncrease(driver, 4, fluentWait);
        utils.addToCartFromTheListWithoutIncrease(driver, 5, fluentWait);
        utils.addToCartFromTheListWithoutIncrease(driver, 6, fluentWait);
        utils.navigateToBags(driver);
        utils.addToCartFromTheListWithIncrease(driver, 1, fluentWait);
        utils.addToCartFromTheListWithIncrease(driver, 2, fluentWait);
        utils.addToCartFromTheListWithIncrease(driver,3, fluentWait);
        utils.addToCartFromTheListWithIncrease(driver,4, fluentWait);
        utils.addToCartFromTheListWithIncrease(driver, 5, fluentWait);
        Assertions.assertEquals(driver.findElement(By.xpath("//*[@id=\"_desktop_cart\"]/div/div/a/span[2]")).getText(), "(15)");
    }

    @Test
    @Order(2)
    public void addProductAfterSearchResultTest(){
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys("czapka");
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/div[1]/article/div/div[1]/a/img")).click();
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[2]/button")).click();
        fluentWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/button")));
        fluentWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/button"))));
        driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/button")).click();
        driver.navigate().back();
        Assertions.assertEquals("(16)", driver.findElement(By.xpath("//*[@id=\"_desktop_cart\"]/div/div/a/span[2]")).getText());
    }

    @Test
    @Order(3)
    public void deletingProductsFromCartTest(){
        driver.findElement(By.xpath("//*[@id=\"_desktop_cart\"]/div/div/a/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[1]/div/div[2]/ul/li[1]/div/div[3]/div/div[3]/div")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[1]/div/div[2]/ul/li[2]/div/div[3]/div/div[3]/div")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[1]/div/div[2]/ul/li[3]/div/div[3]/div/div[3]/div")).click();
        long count = driver.findElements(By.xpath("//*[@class = 'cart-item']")).stream().count();

        Assertions.assertEquals(11, driver.findElements(By.xpath("//*[@class = 'cart-item']")).stream().count());


    }

    @Test
    @Order(4)
    public void registrationTest(){
        driver.findElement(By.xpath("//*[@id=\"_desktop_user_info\"]/div/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"field-id_gender-1\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"field-firstname\"]")).sendKeys("Grzegorz");
        driver.findElement(By.xpath("//*[@id=\"field-lastname\"]")).sendKeys("Fiedoruk");
        driver.findElement(By.xpath("//*[@id=\"field-email\"]")).sendKeys("grzegorz.fiedoruk" + new Random().nextInt(100) +"00@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"field-password\"]")).sendKeys("qwer1234xD");
        driver.findElement(By.xpath("//*[@id=\"field-password\"]")).sendKeys("qwer1234xD");
        driver.findElement(By.xpath("//*[@id=\"customer-form\"]/div/div[8]/div[1]/span/label/input")).click();
        driver.findElement(By.xpath("//*[@id=\"customer-form\"]/div/div[10]/div[1]/span/label/input")).click();
        driver.findElement(By.xpath("//*[@id=\"customer-form\"]/footer/button")).click();
        Assertions.assertEquals("Grzegorz Fiedoruk", driver.findElement(By.xpath("//*[@id=\"_desktop_user_info\"]/div/a[2]/span")).getText());




    }
//
    @Test
    @Order(5)
    public void completingOrderTest(){
        driver.findElement(By.xpath("//*[@id=\"_desktop_cart\"]/div/div/a/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div[1]/div[2]/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"field-address1\"]")).sendKeys("test adres");
        driver.findElement(By.xpath("//*[@id=\"field-postcode\"]")).sendKeys("19-300");
        driver.findElement(By.xpath("//*[@id=\"field-city\"]")).sendKeys("test miasto");
        driver.findElement(By.xpath("//*[@id=\"delivery-address\"]/div/footer/button")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.findElement(By.xpath("//*[@id=\"delivery_option_6\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"js-delivery\"]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"payment-option-2\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"conditions_to_approve[terms-and-conditions]\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"payment-confirmation\"]/div[1]/button")).click();
        Assertions.assertEquals("Potwierdzenie zamówienia", driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div/nav/ol/li[2]/span")).getText());
    }

    @Test
    @Order(6)
    public void checkingOrderStatus(){
        driver.findElement(By.xpath("//*[@id=\"_desktop_user_info\"]/div/a[2]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"history-link\"]/span/i")).click();
        driver.findElement(By.xpath("//*[@id=\"content\"]/table/tbody/tr/td[6]/a[1]")).click();
        Assertions.assertEquals("Oczekiwanie na płatność przy odbiorze", driver.findElement(By.xpath("//*[@id=\"order-history\"]/table/tbody/tr/td[2]/span")).getText());
    }
    @AfterAll
    public static void endTesting() {
        //driver.close();
    }


}
