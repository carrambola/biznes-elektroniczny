package org.example.TestUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class AddMultipleToCartUtils {
    public String clothesCategory;
    public String accesoriesCategory;
    public String dressSubCategory;
    public String bagsSubCategory;
    public String addToCartButton;
    public String continueShoppingButton;
    public String increaseQuantityButton;





    public  AddMultipleToCartUtils(WebDriver driver) {
        clothesCategory = "//*[@id=\"category-50\"]/a";
        accesoriesCategory = "//*[@id=\"category-69\"]/a";
        dressSubCategory ="//*[@id=\"subcategories\"]/ul/li[1]/h5/a";
        bagsSubCategory = "//*[@id=\"subcategories\"]/ul/li[1]/h5/a";
        addToCartButton = "//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[2]/button";
        continueShoppingButton = "//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/button";
        increaseQuantityButton = "//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[1]/div/span[3]/button[1]/i";
    }

    public void navigateToDresses(WebDriver driver){
        driver.findElement(By.xpath(clothesCategory)).click();
        driver.findElement(By.xpath(dressSubCategory)).click();
    }

    public void navigateToBags(WebDriver driver){
        driver.findElement(By.xpath(accesoriesCategory));
        driver.findElement(By.xpath(bagsSubCategory)).click();
    }

    public  void addToCartFromTheListWithoutIncrease(WebDriver driver, int productIndex, FluentWait<WebDriver> fluentWait){
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/div[" + productIndex + "]/article/div/div[1]/a/img")).click();
        driver.findElement(By.xpath(addToCartButton)).click();
        fluentWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(continueShoppingButton))));
        driver.findElement(By.xpath(continueShoppingButton)).click();
        driver.navigate().back();
    }

    public void addToCartFromTheListWithIncrease(WebDriver driver, int productIndex, FluentWait<WebDriver> fluentWait){
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/div[" + productIndex + "]/article/div/div[1]/a/img")).click();
        driver.findElement(By.xpath(increaseQuantityButton)).click();
        driver.findElement(By.xpath(addToCartButton)).click();
        fluentWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(continueShoppingButton))));
        driver.findElement(By.xpath(continueShoppingButton)).click();
        driver.navigate().back();
        driver.navigate().back();
    }
}
