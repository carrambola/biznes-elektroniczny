# biznes-elektroniczny

## Spis treści:
1. Skład zespołu
2. Opis projektu
3. Użyte oprogramowanie
4. Sposób uruchomienia

## Skład zespołu:
1. Grzegorz Fiedoruk 179981
2. Julia Klementowicz 184427
3. Andriy Kaczar 183175
4. Maciej Suliński 176083
5. Marcin Buko 184839

## Opis projektu:

Celem projektu jest imitacja istniejącego sklepu z odzieżą www.mohito.com. Produkty znajdujące się w bazie danych zostały pozyskane metodą scrapowania z oryginalnej strony sklepu. Zbudowany jest na podstawie oprogramowania PrestaShop umożliwijającego tworzenie sklepów z internetowych bez potrzeby kodowania wszystkich funkcjonalności własnoręcznie.

Podgląd jest dostępny pod urlem localhost:8080

Podgląd phpMyAdmin jest dostępny pod ulem localhost:8081

## Użyte oprogramowanie:

1. PrestaShop: 1.7.8.10
2. MariaDB: 11.1.2
3. phpMyAdmin: 5.2.1
4. Docker
5. Java
6. Selenium
7. Javascript

## Sposób uruchomienia:

1. Sklonowanie lokalnie repozytorium
2. W terminalu/konsoli nawigacja do ścieżki, pod którą znajduje się plik docker.compose
3. Wykonanie komendy: "docker compose up --build"



