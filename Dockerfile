FROM prestashop/prestashop:1.7.8

COPY ./apache2.conf /etc/apache2/apache2.conf
COPY ./000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /etc/apache2/sites-enabled/server.key -out /etc/apache2/sites-enabled/server.crt \
    -subj "/C=PL/ST=Pomorskie/L=Gdańsk/O=PG/OU=Informatyka NST/CN=localhost/emailAddress=marcinbuko7@gmail.com" && \
    a2enmod ssl && \
    service apache2 restart 

ENV DB_SERVER: mariadb
ENV DB_NAME: prestashop
ENV DB_USER: root
ENV DB_PASSWD: prestashop
ENV PS_DOMAIN: "localhost:80"
ENV PS_LANGUAGE: pl
ENV PS_COUNTRY: PL
ENV PS_FOLDER_ADMIN: admin4577
ENV ADMIN_MAIL: "marcinbuko7@gmail.com"
ENV ADMIN_PASSWD: prestashop


EXPOSE 80
EXPOSE 443

